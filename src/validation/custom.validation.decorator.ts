import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';
import {
  ArgumentMetadata,
  BadRequestException,
  PipeTransform,
} from '@nestjs/common';

@ValidatorConstraint({ async: true })
export class IsTrueEmailConstraint implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments) {
    if (typeof value === 'string') {
      const emailParts = value.match(/@(.+)$/);
      if (emailParts && emailParts[1] === 'gmail.com') return true;
    }
    return false;
  }
}

export function IsTrueEmail(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'isTrueEmail',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsTrueEmailConstraint,
    });
  };
}

export class IdValidationPipe implements PipeTransform {
  async transform(value, metadata: ArgumentMetadata) {
    if (metadata.type === 'param') {
      // This is the relevant part: value -> { id: value }
      try {
        const id = parseInt(value);
        if (id >= 0) return parseInt(value);
        else {
          throw new BadRequestException('Invalid param');
        }
      } catch {
        throw new BadRequestException('Invalid param');
      }
    } else {
      return value;
    }
  }
}
