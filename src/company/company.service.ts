import { Inject, Injectable, UseInterceptors } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Company } from './Company.entity';
import { DatabaseService } from 'src/database/database.service';
@Injectable()
export class CompanyService extends DatabaseService<Company> {
  constructor(
    @Inject('COMPANY_REPOSITORY')
    private readonly companyRepository: Repository<Company>,
  ) {
    super(companyRepository);
  }
}
