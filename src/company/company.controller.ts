import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  NotFoundException,
} from '@nestjs/common';
import { CompanyService } from './company.service';
import { Company } from './Company.entity';

@Controller('company')
export class CompanyController {
  constructor(private readonly companyService: CompanyService) {}

  //get all
  @Get()
  async findAll(): Promise<Company[]> {
    return this.companyService.findAll();
  }

  // get  by id
  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Company> {
    const user = await this.companyService.findOne(id);
    if (!user) {
      throw new NotFoundException('User does not exist!');
    } else {
      return user;
    }
  }

  //create
  @Post()
  async create(@Body() user: Company): Promise<Company> {
    return this.companyService.create(user);
  }

  // update
  @Put(':id')
  async update(@Param('id') id: number, @Body() user: Company): Promise<any> {
    return this.companyService.update(id, user);
  }

  //delete
  @Delete(':id')
  async delete(@Param('id') id: number): Promise<any> {
    //handle error if user does not exist
    const user = await this.companyService.findOne(id);
    if (!user) {
      throw new NotFoundException('User does not exist!');
    }
    return this.companyService.delete(id);
  }
}
