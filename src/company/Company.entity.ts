import { Agency } from 'src/agency/Agency';
import { Department } from 'src/department/department.entity';
import { User } from 'src/user/User.entity';

import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

@Entity()
export class Company {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name_company: string;

  @Column()
  address: string;

  @OneToMany(() => Department, (department) => department.company)
  department: Department[];

  @OneToMany(() => Agency, (agency) => agency.company)
  agency: Agency[];
}
