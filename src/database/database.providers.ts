import { DataSource } from 'typeorm';
import { User } from '../user/User.entity';
import { Agency } from 'src/agency/Agency';
import { Company } from 'src/company/Company.entity';
import { Position } from 'src/position/Position.entity';
import { Department } from 'src/department/department.entity';

export const databaseProviders = [
  {
    provide: 'DATA_SOURCE',
    useFactory: async () => {
      const dataSource = new DataSource({
        type: 'postgres',
        host: 'localhost',
        port: 5432,
        username: 'customuser',
        password: 'custompassword',
        database: 'dbname',
        synchronize: true,
        logging: 'all',
        logger: 'advanced-console',
        entities: [User, Company, Agency, Position, Department],
        migrations: [],
        subscribers: [],
      });
      return dataSource.initialize();
    },
  },
];
