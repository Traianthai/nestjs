import { UserService } from '../user/user.service';
import { forwardRef, Inject, UnauthorizedException } from '@nestjs/common';
import { User } from '../user/User.entity';
import { JwtService } from '@nestjs/jwt';
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
    private jwtService: JwtService,
  ) {}
  async signIn(username: string, pass: string) {
    const user: User = await this.userService.findByUsernametoLogin(username);
    console.log(user);
    if (user?.pass !== pass) {
      throw new UnauthorizedException({
        error: 'Incorrect username or password',
      });
    }
    const payload = {
      sub: user.id,
      username: user.firstName,
      roles: user.roles,
    };
    return {
      access_token: await this.jwtService.signAsync(payload),
      roles: user.roles,
    };
  }
  // async  signOut(){
  //     this.jwtService.
  // }
}
