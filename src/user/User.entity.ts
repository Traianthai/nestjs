import { Expose, Transform, Type } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsDateString,
  IsEmpty,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
  isString,
  IsIn,
} from 'class-validator';
import { Company } from 'src/company/Company.entity';
import { Position } from 'src/position/Position.entity';
import { IsTrueEmail } from 'src/validation/custom.validation.decorator';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  ManyToMany,
  JoinTable,
  CreateDateColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  @IsString()
  @Expose()
  firstName: string;

  @Column()
  @IsString()
  @IsNotEmpty()
  @Expose()
  lastName: string;

  @Column()
  @IsNotEmpty()
  @IsInt()
  @Expose()
  age: number;

  @Column({ nullable: true, default: null, type: 'date' })
  @IsNotEmpty()
  @IsDateString()
  @Expose()
  dateofbirth: Date;

  @Column({ nullable: true, default: null })
  @IsNotEmpty()
  @IsTrueEmail({
    message: 'wrong email format.',
  })
  @Expose()
  email: string;

  @ManyToMany(() => Position, { cascade: true })
  @ValidateNested({ each: true })
  @Type(() => Position)
  @JoinTable({
    name: 'user_position',
    joinColumn: { name: 'user_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'position_id' },
  })
  position?: Position[];

  @Column({
    nullable: true,
    default: 'user',
    type: 'enum',
    enum: ['admin', 'user'],
  })
  @IsIn(['admin', 'user'])
  @IsNotEmpty()
  @Expose()
  roles: string;

  @Column({
    default: 'null',
    nullable: true,
  })
  @Expose()
  @IsNotEmpty()
  pass: string;

  // @Expose()
  // @Transform(({obj}) => obj.firstName +" " + obj.lastName + " " + obj.age + " " + obj.email)
  // getString: string

  // @Expose()
  // @IsNotEmpty()
  // @Type(() => String)
  // @Transform(({value}) => {
  //     console.log(1)
  //     if(value && typeof value === 'string') return value.split(' ');
  //     else return value
  // },{ toClassOnly: true })
  // getArray: string[]
}
