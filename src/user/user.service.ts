import { Inject, Injectable, NotFoundException, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from './User.entity';
import { DatabaseService } from 'src/database/database.service';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class UserService extends DatabaseService<User> {
  constructor(
    @Inject('USER_REPOSITORY')
    private readonly userRepository: Repository<User>,
    private mailerService: MailerService,
  ) {
    super(userRepository);
  }

  private readonly logger = new Logger(UserService.name);

  // @Cron("30 15 * * *")
  // @Cron(CronExpression.EVERY_10_SECONDS)
  async handleCron() {
    const date = new Date();

    const users = await this.userRepository
      .createQueryBuilder('user')
      .where('EXTRACT(DAY FROM dateofbirth) = :day', { day: date.getDate() })
      .andWhere('EXTRACT(MONTH FROM dateofbirth) = :month', {
        month: date.getMonth() + 1,
      })
      .getMany();

    users.forEach((user) => {
      const email: string = user.email;
      this.mailerService.sendMail({
        to: email,
        subject: 'Happy birthday bro',
        template: './happybirthday',
        context: {
          name: user.firstName,
        },
      });
      this.logger.log('Sent to !' + user.firstName);
    });
  }
  async getUserbyDate(date: number, month: number) {
    const user = this.userRepository
      .createQueryBuilder('user')
      .where('EXTRACT(DAY FROM dateofbirth) = :day', { day: date })
      .andWhere('EXTRACT(MONTH FROM dateofbirth) = :month', { month: month })
      .getMany();
    return user;
  }

  async findByUsername(firstName: string): Promise<User[] | undefined> {
    const user = this.userRepository
      .createQueryBuilder('user')
      .where('user.firstName LIKE :firstName ', { firstName: firstName + '%' })
      .getMany();
    return user;
  }

  async findByUsernametoLogin(firstName: string): Promise<User> | undefined {
    const user = this.userRepository
      .createQueryBuilder('user')
      .where('user.firstName = :firstName ', { firstName: firstName })
      .getOne();
    return user;
  }

  async findByPositionName(name_position: string): Promise<User[] | undefined> {
    const user = this.userRepository
      .createQueryBuilder('user')
      .innerJoinAndSelect(
        'user.position',
        'position',
        'position.name_position LIKE :name_position',
        { name_position: name_position + '%' },
      )
      .getMany();
    return user;
  }

  async findAllByPositionName(
    name_position: string,
  ): Promise<User[] | undefined> {
    const user = this.userRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect(
        'user.position',
        'position',
        'position.name_position LIKE :name_position',
        { name_position: name_position + '%' },
      )
      .getMany();
    return user;
  }

  async getAll(): Promise<User[] | undefined> {
    const user = this.userRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.position', 'position')
      .getMany();
    return user;
  }

  async findAllByNameorPosition(topic: string): Promise<User[] | undefined> {
    const user = this.userRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.position', 'position')
      .where(
        'position.name_position LIKE :topic OR user.firstName LIKE :topic OR user.lastName LIKE :topic',
        { topic: '%' + topic + '%' },
      )
      .getMany();
    return user;
  }

  async getOne(id_user: number): Promise<User> {
    return this.userRepository
      .createQueryBuilder('user')
      .where('user.id = :id', { id: id_user })
      .leftJoinAndSelect('user.position', 'position')
      .getOne();
  }

  async addPosition(id_positions: number[], id_user: number): Promise<User> {
    const data: { user_id: number; position_id: number }[] = [];
    id_positions.map((idex) => {
      data.push({ user_id: id_user, position_id: idex });
    });
    console.log(id_positions);
    console.log(data);
    try {
      await this.userRepository
        .createQueryBuilder('user_position')
        .insert()
        .into('user_position')
        .values(data)
        .execute();
    } catch (err) {
      throw new NotFoundException('Position exist!');
    }

    return this.userRepository
      .createQueryBuilder('user')
      .where('user.id = :id', { id: id_user })
      .leftJoinAndSelect('user.position', 'position')
      .getOne();
  }

  async delPostion(id_positions: number[], id_user: number): Promise<User> {
    if (id_positions.length != 0) {
      await this.userRepository
        .createQueryBuilder('user_position')
        .delete()
        .from('user_position')
        .where('position_id IN(:...ids) AND user_id = :id_user', {
          ids: id_positions,
          id_user: id_user,
        })
        .execute();
    }

    return this.userRepository
      .createQueryBuilder('user')
      .where('user.id = :id', { id: id_user })
      .leftJoinAndSelect('user.position', 'position')
      .getOne();
  }

  async updatePosition(
    id_positions_old: number,
    id_positions_new: number,
    user_id: number,
  ): Promise<User> {
    console.log(
      await this.userRepository
        .createQueryBuilder('user_position')
        .update('user_position')
        .set({
          position_id: id_positions_new,
          user_id: user_id,
        })
        .where('position_id = :id_positions_old AND user_id = :user_id', {
          id_positions_old: id_positions_old,
          user_id: user_id,
        })
        .execute(),
    );

    return this.userRepository
      .createQueryBuilder('user')
      .where('user.id = :id', { id: user_id })
      .leftJoinAndSelect('user.position', 'position')
      .getOne();
  }
}
