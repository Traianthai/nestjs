import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  NotFoundException,
  ParseArrayPipe,
  UsePipes,
  ValidationPipe,
  Request,
  UseGuards,
} from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './User.entity';
import { IsInt, IsNotEmpty } from 'class-validator';
import { IdValidationPipe } from 'src/validation/custom.validation.decorator';
import { UserDecorator } from './user.decorator';
import { Auth } from '../auth/auth.decorator';
import { AuthGuard } from '../auth/auth.guard';
import { plainToClass } from 'class-transformer';

export class UpdateIdPosition {
  @IsInt()
  @IsNotEmpty()
  id_positions_old: number;

  @IsInt()
  @IsNotEmpty()
  id_positions_new: number;
}

@Controller('user')
@UsePipes(
  new ValidationPipe({
    whitelist: true,
    validateCustomDecorators: true,
    // transform: true
  }),
)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('getbydate')
  async getUserbyDate(
    @Body() date: { day: number; month: number },
  ): Promise<User[]> {
    console.log(date);
    return this.userService.getUserbyDate(date.day, date.month);
  }

  //get all users
  @Auth('admin')
  @Get('getall')
  async findAll(): Promise<User[]> {
    return this.userService.getAll();
  }

  @Auth('admin', 'user')
  @Get('profile')
  async profile(@Request() req): Promise<User> {
    const id = req.user.sub;
    return this.userService.getOne(id);
  }

  @Auth('user')
  @Get('getall/:id')
  @UsePipes(IdValidationPipe)
  async getOne(@Param('id') id: number): Promise<User> {
    return this.userService.getOne(id);
  }

  @Auth('admin')
  @Get()
  async getAll(): Promise<User[]> {
    return this.userService.findAll();
  }

  @Auth('admin')
  @Get('getbyname/:name')
  async findbyname(@Param('name') name: string): Promise<User[]> {
    const user = await this.userService.findByUsername(name);
    if (!user) {
      throw new NotFoundException('User does not exist!');
    } else {
      return user;
    }
  }

  @Auth('admin')
  @Get('getbyPositionName/:name')
  async findByPositionName(@Param('name') name: string): Promise<User[]> {
    const user = await this.userService.findByPositionName(name);
    if (!user) {
      throw new NotFoundException('User does not exist!');
    } else {
      return user;
    }
  }

  @Get('getAllbyPositionName/:name')
  async findAllByPositionName(@Param('name') name: string): Promise<User[]> {
    const user = await this.userService.findAllByPositionName(name);
    if (!user) {
      throw new NotFoundException('User does not exist!');
    } else {
      return user;
    }
  }

  @Auth('admin')
  @Get('getAllByNameorPosition/:topic')
  async findAllByNameorPosition(
    @Param('topic') topic: string,
  ): Promise<User[]> {
    const user = await this.userService.findAllByNameorPosition(topic);
    if (!user) {
      throw new NotFoundException('User does not exist!');
    } else {
      return user;
    }
  }
  // get user by id
  @Get(':id')
  @UsePipes(IdValidationPipe)
  async findOne(@Param('id') id: number): Promise<User> {
    const user = await this.userService.findOne(id);
    if (!user) {
      throw new NotFoundException('User does not exist!');
    } else {
      return user;
    }
  }

  //create user
  @Post()
  @Auth('admin')
  async create(@UserDecorator() user: User): Promise<User> {
    const userReal = plainToClass(User, user, {
      excludeExtraneousValues: true,
    });
    return this.userService.create(userReal);
    // return userReal
  }

  @Auth('admin')
  @Post(':id')
  @UsePipes(IdValidationPipe)
  async addPosition(
    @Body(new ParseArrayPipe({ items: Number })) position: number[],
    @Param('id') id_user: number,
  ): Promise<User> {
    const user = this.userService.addPosition(position, id_user);
    return user;
  }

  // update user
  @Put(':id')
  @UsePipes(IdValidationPipe)
  async update(@Param('id') id: number, @Body() user: User): Promise<User> {
    return this.userService.update(id, user);
  }

  //delete user
  @Delete(':id')
  @UsePipes(IdValidationPipe)
  async delete(@Param('id') id: number): Promise<void> {
    //handle error if user does not exist
    const user = await this.userService.findOne(id);
    if (!user) {
      throw new NotFoundException('User does not exist!');
    }
    return this.userService.delete(id);
  }
  @Delete('/delposition/:id')
  @UsePipes(IdValidationPipe)
  async delPostion(
    @Body(new ParseArrayPipe({ items: Number })) position: number[],
    @Param('id') id_user: number,
  ): Promise<User> {
    return this.userService.delPostion(position, id_user);
  }

  @Put('updatePosition/:id')
  @UsePipes(IdValidationPipe)
  async updatePosition(
    @Body() position: UpdateIdPosition,
    @Param('id') id_user: number,
  ): Promise<User> {
    return this.userService.updatePosition(
      position.id_positions_old,
      position.id_positions_new,
      id_user,
    );
  }
}
