import { DataSource } from 'typeorm';
import { Position } from './Position.entity';

export const positionProvider = [
  {
    provide: 'POSITION_REPOSITORY',
    useFactory: (dataSource: DataSource) => dataSource.getRepository(Position),
    inject: ['DATA_SOURCE'],
  },
];
