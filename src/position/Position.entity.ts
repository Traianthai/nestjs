import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { User } from 'src/user/User.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';

@Entity()
export class Position {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  name_position: string;

  @ManyToMany(() => User, { cascade: true })
  @ValidateNested({ each: true })
  @Type(() => User)
  @JoinTable({
    name: 'user_position',
    joinColumn: { name: 'position_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'user_id' },
  })
  user: User[];
}
