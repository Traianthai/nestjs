import { Inject, Injectable, UseInterceptors } from '@nestjs/common';
import { BaseEntity, Repository } from 'typeorm';
import { Position } from './Position.entity';
import { DatabaseService } from 'src/database/database.service';
import { User } from 'src/user/User.entity';
BaseEntity;
@Injectable()
export class PositionService extends DatabaseService<Position> {
  constructor(
    @Inject('POSITION_REPOSITORY')
    private readonly positionRepository: Repository<Position>,
  ) {
    super(positionRepository);
  }

  async findByPositionname(
    name_position: string,
  ): Promise<Position[] | undefined> {
    const position = this.positionRepository
      .createQueryBuilder('position')
      .where('position.name_position LIKE :name_position ', {
        name_position: name_position + '%',
      })
      .getMany();
    return position;
  }

  async getAll(): Promise<Position[]> {
    const position = this.positionRepository
      .createQueryBuilder('position')
      .leftJoinAndSelect('position.user', 'user')
      .getMany();
    return position;
  }

  async addUser(id_users: number[], id: number): Promise<Position> {
    const data: { user_id: number; position_id: number }[] = [];
    id_users.map((i) => {
      data.push({ user_id: i, position_id: id });
    });
    console.log(id_users);
    console.log(data);
    await this.positionRepository
      .createQueryBuilder('user_position')
      .insert()
      .into('user_position')
      .values(data)
      .execute();

    return this.positionRepository
      .createQueryBuilder('position')
      .where('position.id = :id', { id: id })
      .leftJoinAndSelect('position.user', 'user')
      .getOne();
  }
}
