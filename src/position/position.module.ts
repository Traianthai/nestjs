import { Module } from '@nestjs/common';
import { PositionController } from './position.controller';
import { PositionService } from './position.service';
import { DatabaseModule } from 'src/database/database.module';
import { positionProvider } from './position.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [PositionController],
  providers: [...positionProvider, PositionService],
})
export class PositionModule {}
