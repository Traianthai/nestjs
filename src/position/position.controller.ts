import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  NotFoundException,
  ParseIntPipe,
  UsePipes,
  ValidationPipe,
  ParseArrayPipe,
} from '@nestjs/common';
import { PositionService } from './position.service';
import { Position } from './Position.entity';
import { isArray } from 'class-validator';

@Controller('position')
@UsePipes(
  new ValidationPipe({
    whitelist: true,
    transform: true,
  }),
)
export class PositionController {
  constructor(private readonly positionService: PositionService) {}

  @Get('test')
  async test(@Body() position: Position) {
    return position;
  }

  //get all position
  @Get('getall')
  async getAll(): Promise<Position[]> {
    return this.positionService.getAll();
  }

  @Get()
  async findAll(): Promise<Position[]> {
    return this.positionService.findAll();
  }

  @Get('getbyname/:name')
  async findbyname(@Param('name') name: string): Promise<Position[]> {
    const position = await this.positionService.findByPositionname(name);
    if (!position) {
      throw new NotFoundException('position does not exist!');
    } else {
      return position;
    }
  }
  // get position by id
  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number): Promise<Position> {
    const position = await this.positionService.findOne(id);
    if (!position) {
      throw new NotFoundException('User does not exist!');
    } else {
      return position;
    }
  }

  //create position
  @Post(':id')
  async addUser(
    @Body(ParseArrayPipe) id_users: number[],
    @Param('id', ParseIntPipe) id: number,
  ): Promise<Position> {
    return this.positionService.addUser(id_users, id);
  }

  @Post()
  async create(@Body() position: Position): Promise<Position> {
    return this.positionService.create(position);
  }

  // update position
  @Put(':id')
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() position: Position,
  ): Promise<Position> {
    return this.positionService.update(id, position);
  }

  //delete
  @Delete(':id')
  async delete(@Param('id', ParseIntPipe) id: number): Promise<void> {
    //handle error if user does not exist
    const position = await this.positionService.findOne(id);
    if (!position) {
      throw new NotFoundException('Position does not exist!');
    }
    return this.positionService.delete(id);
  }
}
