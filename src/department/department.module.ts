import { Module } from '@nestjs/common';

import { DatabaseModule } from 'src/database/database.module';
import { departmentProviders } from './department.providers';
import { DepartmentService } from './department.service';
import { DepartmentController } from './department.controller';

@Module({
  imports: [DatabaseModule],
  controllers: [DepartmentController],
  providers: [...departmentProviders, DepartmentService],
})
export class DepartmentModule {}
