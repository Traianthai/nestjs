import { Inject, Injectable, UseInterceptors } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Department } from './department.entity';
import { DatabaseService } from 'src/database/database.service';
@Injectable()
export class DepartmentService extends DatabaseService<Department> {
  constructor(
    @Inject('DEPARTMENT_REPOSITORY')
    private readonly departmentRepository: Repository<Department>,
  ) {
    super(departmentRepository);
  }

  async getAllbyCompanyNameandAgencyName(
    name_company: string,
    name_agency: string,
  ): Promise<Department[] | undefined> {
    const user = this.departmentRepository
      .createQueryBuilder('department')
      .leftJoinAndSelect(
        'department.company',
        'company',
        'company.name_company LIKE :name_company',
        { name_company: name_company + '%' },
      )
      .leftJoinAndSelect(
        'department.agency',
        'agency',
        'agency.name_agency LIKE :name_agency',
        { name_agency: name_agency + '%' },
      )
      .getMany();
    return user;
  }
}
