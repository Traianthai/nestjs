import { Agency } from 'src/agency/Agency';
import { Company } from 'src/company/Company.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity()
export class Department {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name_department: string;

  @ManyToOne(() => Company, (company) => company.department)
  company: Company;

  @ManyToOne(() => Agency, (agency) => agency.department)
  agency: Agency;
}
