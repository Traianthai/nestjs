import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  NotFoundException,
} from '@nestjs/common';
import { DepartmentService } from './department.service';
import { Department } from './department.entity';

@Controller('department')
export class DepartmentController {
  constructor(private readonly companyService: DepartmentService) {}

  @Get('getAllbyCompanyNameandAgencyName')
  async getAllbyCompanyNameandAgencyName(
    @Body() name: { name_company: string; name_agency: string },
  ): Promise<Department[]> {
    const department =
      await this.companyService.getAllbyCompanyNameandAgencyName(
        name.name_company,
        name.name_agency,
      );
    if (!department) {
      throw new NotFoundException('department does not exist!');
    } else {
      return department;
    }
  }
  //get all
  @Get()
  async findAll(): Promise<Department[]> {
    return this.companyService.findAll();
  }
  // get  by id
  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Department> {
    const department = await this.companyService.findOne(id);
    if (!department) {
      throw new NotFoundException('department does not exist!');
    } else {
      return department;
    }
  }

  //create
  @Post()
  async create(@Body() department: Department): Promise<Department> {
    return this.companyService.create(department);
  }

  // update
  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() department: Department,
  ): Promise<any> {
    return this.companyService.update(id, department);
  }

  //delete
  @Delete(':id')
  async delete(@Param('id') id: number): Promise<any> {
    //handle error if department does not exist
    const department = await this.companyService.findOne(id);
    if (!department) {
      throw new NotFoundException('department does not exist!');
    }
    return this.companyService.delete(id);
  }
}
