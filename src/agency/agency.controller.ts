import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  NotFoundException,
} from '@nestjs/common';
import { AgencyService } from './agency.service';
import { Agency } from './Agency';
@Controller('agency')
export class AgencyController {
  constructor(private readonly positionService: AgencyService) {}

  //get all users
  @Get()
  async findAll(): Promise<Agency[]> {
    return this.positionService.findAll();
  }
  @Get('getbyname/:name')
  async findbyname(@Param('name') name: string): Promise<Agency[]> {
    const user = await this.positionService.findByAgencyname(name);
    if (!user) {
      throw new NotFoundException('User does not exist!');
    } else {
      return user;
    }
  }
  // get user by id
  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Agency> {
    const user = await this.positionService.findOne(id);
    if (!user) {
      throw new NotFoundException('User does not exist!');
    } else {
      return user;
    }
  }

  //create user
  @Post()
  async create(@Body() user: Agency): Promise<Agency> {
    return this.positionService.create(user);
  }

  // update user
  @Put(':id')
  async update(@Param('id') id: number, @Body() user: Agency): Promise<any> {
    return this.positionService.update(id, user);
  }

  //delete user
  @Delete(':id')
  async delete(@Param('id') id: number): Promise<any> {
    //handle error if user does not exist
    const user = await this.positionService.findOne(id);
    if (!user) {
      throw new NotFoundException('User does not exist!');
    }
    return this.positionService.delete(id);
  }
}
