import { DataSource } from 'typeorm';
import { Agency } from './Agency';

export const agencyProvider = [
  {
    provide: 'AGENCY_REPOSITORY',
    useFactory: (dataSource: DataSource) => dataSource.getRepository(Agency),
    inject: ['DATA_SOURCE'],
  },
];
