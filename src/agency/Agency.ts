import { Company } from 'src/company/Company.entity';
import { Department } from 'src/department/department.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Agency {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name_agency: string;

  @ManyToOne(() => Company, (company) => company.agency)
  company: Company;

  @OneToMany(() => Department, (department) => department.agency)
  department: Department[];
}
