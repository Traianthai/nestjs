import { Module } from '@nestjs/common';
import { AgencyController } from './agency.controller';
import { AgencyService } from './agency.service';
import { DatabaseModule } from 'src/database/database.module';
import { agencyProvider } from './agency.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [AgencyController],
  providers: [...agencyProvider, AgencyService],
})
export class AgencyModule {}
