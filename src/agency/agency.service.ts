import { Inject, Injectable, UseInterceptors } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Agency } from './Agency';
import { DatabaseService } from 'src/database/database.service';
@Injectable()
export class AgencyService extends DatabaseService<Agency> {
  constructor(
    @Inject('AGENCY_REPOSITORY')
    private readonly userRepository: Repository<Agency>,
  ) {
    super(userRepository);
  }

  async findByAgencyname(name_agency: string): Promise<Agency[] | undefined> {
    const user = this.userRepository
      .createQueryBuilder('agency')
      .where('agency.name_agency LIKE :name_agency ', {
        name_agency: name_agency + '%',
      })
      .getMany();
    return user;
  }

  async findByCompanyName(CompanyName: string): Promise<Agency[] | undefined> {
    const user = this.userRepository
      .createQueryBuilder('user')
      .innerJoinAndSelect(
        'user.company',
        'company',
        'company.name_company LIKE :name_company',
        { name_company: CompanyName + '%' },
      )
      .getMany();
    return user;
  }

  async findAllByCompanyName(
    CompanyName: string,
  ): Promise<Agency[] | undefined> {
    const user = this.userRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect(
        'user.company',
        'company',
        'company.name_company LIKE :name_company',
        { name_company: CompanyName + '%' },
      )
      .getMany();
    return user;
  }
}
